﻿using Control.Pad;
using Control.Pad.Mock;
using Control.Pad.Xbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PadTypeCollector padTypeCollector;
        private List<ButtonRow> buttonsList = new List<ButtonRow>();

        public MainWindow()
        {
            InitializeComponent();
            padTypeCollector = new PadTypeCollector();

            padTypeCollector.AddPadType(new XboxPadManager());
            padTypeCollector.AddPadType(new PadMockManager());

            padTypeCollector.Init();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var pads = padTypeCollector.GetAvailablePads();

            listBox.Items.Clear();
            foreach (var pad in pads)
            {
                listBox.Items.Add(pad);
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if(listBox.SelectedIndex == -1)
                return;

            var pad = (Pad) listBox.SelectedItem;

            listBox1.Items.Clear();
            
            foreach (var button in pad.Buttons)
            {
                listBox1.Items.Add(button.FullName);

                var br = new ButtonRow(button.FullName, false);
                buttonsList.Add(br);
            }

            dataGrid.ItemsSource = buttonsList;
        }

        private void buttonClick(PadController sender, PadController.ButtonEventArgs e)
        {
            listBox2.Items.Insert(0, e.Button.FullName + " click!");
            buttonsList.FirstOrDefault(x => x.Name == e.Button.FullName).Pressed = true;

            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = buttonsList;
        }

        private void buttonRelease(PadController sender, PadController.ButtonEventArgs e)
        {
            listBox2.Items.Insert(0, e.Button.FullName + " release!");
            buttonsList.FirstOrDefault(x => x.Name == e.Button.FullName).Pressed = false;

            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = buttonsList;
        }

        private void analogUpdate(PadController sender, PadController.AnalogEventArgs e)
        {
            label.Content = e.Analog.Name + ": " + e.Value;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (listBox.SelectedIndex == -1)
                return;

            var pad = (Pad)listBox.SelectedItem;

            var controller = padTypeCollector.GetController(pad);
            controller.Start();

            label.Content = controller.ToString();
            controller.OnClick += this.buttonClick;
            controller.OnRelease += this.buttonRelease;
            controller.OnAnalogChanged += this.analogUpdate;
        }
    }
}
