﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    class ButtonRow
    {
        public string Name { get; set; }
        public bool Pressed { get; set; }

        public ButtonRow(string name, bool pressed)
        {
            this.Name = name;
            this.Pressed = pressed;
        }
    }
}
