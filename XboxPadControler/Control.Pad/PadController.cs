﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad
{
    public abstract class PadController
    {
        public abstract Pad Pad { get; }
        
        public abstract ReadOnlyDictionary<Button, bool> Buttons { get; }
        public abstract ReadOnlyDictionary<Analog, double> Analogs { get; }

        public event ButtonHandler OnClick;
        public event ButtonHandler OnRelease;
        public event AnalogHandler OnAnalogChanged;

        protected void Click(ButtonEventArgs e)
        {
            OnClick?.Invoke(this, e);
        }

        protected void Release(ButtonEventArgs e)
        {
            OnRelease?.Invoke(this, e);
        }

        protected void AnalogChanged(AnalogEventArgs e)
        {
            OnAnalogChanged?.Invoke(this, e);
        }

        public abstract void Start();
        public abstract void Stop();

        public delegate void ButtonHandler(PadController sender, ButtonEventArgs e);
        public delegate void AnalogHandler(PadController sender, AnalogEventArgs e);

        public class AnalogEventArgs : EventArgs
        {
            public AnalogEventArgs(Analog analog, double value)
            {
                Analog = analog;
                Value = value;
            }
            public Analog Analog { get; private set; }
            public double Value { get; private set; }
        }

        public class ButtonEventArgs : EventArgs
        {
            public ButtonEventArgs(Button button)
            {
                Button = button;
            }
            public Button Button { get; private set; }
        }
    }
}
