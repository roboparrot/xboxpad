﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad
{
    public class Button
    {
        public Button(string name, string fullName)
        {
            Name = name;
            FullName = fullName;
        }

        public string Name { get; protected set; }
        public string FullName { get; protected set; }
    }
}
