﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad
{
    public abstract class PadManager
    {
        public abstract void Init();
        public abstract IEnumerable<Pad> GetAvailablePads();
        public abstract PadController GetController(Pad pad);
    }
}
