﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad
{
    public class PadTypeCollector
    {
        IList<PadManager> _padManagers = new List<PadManager>();

        public void AddPadType(PadManager padManager)
        {
            _padManagers.Add(padManager);
        }

        public IList<Pad> GetAvailablePads()
        {
            var padList = new List<Pad>();

            foreach (var padManager in _padManagers)
            {
                var pads = padManager.GetAvailablePads();

                padList.AddRange(pads);
            }

            return padList;
        }

        public void Init()
        {
            foreach (var padManager in _padManagers)
            {
                padManager.Init();
            }
        }

        public PadController GetController(Pad pad)
        {
            foreach (var padManager in _padManagers)
            {
                var padController = padManager.GetController(pad);

                if (padController != null)
                    return padController;
            }

            return null;
        }
    }
}
