﻿namespace Control.Pad
{
    public class Analog
    {
        public string Name { get; set; }

        public Analog(string name)
        {
            Name = name;
        }
    }
}