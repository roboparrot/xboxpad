﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad
{
    public abstract class Pad
    {
        public string Name { get; protected set; }
        
        public abstract ReadOnlyCollection<Button> Buttons { get; }
        public abstract ReadOnlyCollection<Analog> Analogs { get; }

        public override string ToString()
        {
            return Name;
        }
    }
}
