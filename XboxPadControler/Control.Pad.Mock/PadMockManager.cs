﻿using Control.Pad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad.Mock
{
    public class PadMockManager : PadManager
    {
        public override IEnumerable<Pad> GetAvailablePads()
        {
            return new List<Pad>()
            {
                new MockPad("Mock Pad 1"),
                new MockPad("Mock Pad 2")
            };
        }

        public override PadController GetController(Pad pad)
        {
            throw new NotImplementedException();
        }

        public override void Init()
        {
            
        }
    }
}
