﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Control.Pad.Mock
{
    public class MockPad : Pad
    {
        public MockPad(string name)
        {
            _buttons.Add(new Button("B1", "Button 1"));
            _buttons.Add(new Button("B2", "Button 2"));
            _buttons.Add(new Button("B3", "Button 3"));
            _buttons.Add(new Button("B4", "Button 4"));
            _buttons.Add(new Button("B5", "Button 5"));

            _analogs.Add(new Analog("Joystick 1 X"));
            _analogs.Add(new Analog("Joystick 1 Y"));
            _analogs.Add(new Analog("Joystick 2 X"));
            _analogs.Add(new Analog("Joystick 2 Y"));

            Name = name;
        }

        private List<Button> _buttons = new List<Button>();
        public override ReadOnlyCollection<Button> Buttons => new ReadOnlyCollection<Button>(_buttons);


        private List<Analog> _analogs = new List<Analog>();
        public override ReadOnlyCollection<Analog> Analogs => new ReadOnlyCollection<Analog>(_analogs);
    }
}
