﻿using Control.Pad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.XInput;

namespace Control.Pad.Xbox
{
    public class XboxPadManager : PadManager
    {
        private IDictionary<XboxPad, XboxPadController> pads = new Dictionary<XboxPad, XboxPadController>();

        public override void Init()
        {
            pads.Add(new XboxPad(UserIndex.One), null);
            pads.Add(new XboxPad(UserIndex.Two), null);
            pads.Add(new XboxPad(UserIndex.Three), null);
            pads.Add(new XboxPad(UserIndex.Four), null);
        }

        public override IEnumerable<Pad> GetAvailablePads()
        {
            return pads
                .Select(x => x.Key)
                .Where(x => x.Connected)
                .ToList();
        }

        public override PadController GetController(Pad pad)
        {
            if(!pads
                .Select(x=>x.Key)
                .Any(x=>x.Equals(pad))
                )
                return null;

            var xboxPad = (XboxPad) pad;
            var controller = pads[xboxPad];

            if (controller == null)
            {
                controller = pads[xboxPad] = new XboxPadController(xboxPad);
            }

            return controller;
        }
    }
}
