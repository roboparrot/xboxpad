﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.XInput;

namespace Control.Pad.Xbox
{
    public class XboxPad : Pad
    {
        private Controller _controller;

        public Controller Controller { get { return _controller; } }

        public XboxPad(UserIndex index)
        {
            foreach (var val in Enum.GetValues(typeof(GamepadButtonFlags)))
            {
                var name = Enum.GetName(typeof(GamepadButtonFlags), val);

                _buttons.Add(new XboxButton(name, name, (short)val));
            }

            _analogs.Add(new XboxAnalog("LeftThumbX", "LeftThumbX"));
            _analogs.Add(new XboxAnalog("LeftThumbY", "LeftThumbY"));
            _analogs.Add(new XboxAnalog("RightThumbX", "RightThumbX"));
            _analogs.Add(new XboxAnalog("RightThumbY", "RightThumbY"));
            _analogs.Add(new XboxAnalog("LeftTrigger", "LeftTrigger"));
            _analogs.Add(new XboxAnalog("RightTrigger", "RightTrigger"));

            _controller = new Controller(index);
            Name = "Xbox pad " + index;
        }

        public bool Connected => _controller.IsConnected;
        


        private List<XboxButton> _buttons= new List<XboxButton>();
        public override ReadOnlyCollection<Button> Buttons =>
            new ReadOnlyCollection<Button>(_buttons.ToList<Button>());
        public ReadOnlyCollection<XboxButton> XboxButtons =>
            new ReadOnlyCollection<XboxButton>(_buttons);

        private List<XboxAnalog> _analogs = new List<XboxAnalog>();
        public override ReadOnlyCollection<Analog> Analogs =>
            new ReadOnlyCollection<Analog>(_analogs.ToList<Analog>());
        public ReadOnlyCollection<XboxAnalog> XboxAnalogs =>
            new ReadOnlyCollection<XboxAnalog>(_analogs);

    }
}
