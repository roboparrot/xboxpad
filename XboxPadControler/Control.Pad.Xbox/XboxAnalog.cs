﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad.Xbox
{
    public class XboxAnalog : Analog
    {
        public string XboxName { get; set; }

        public XboxAnalog(string name, string xboxName) : base(name)
        {
            XboxName = xboxName;
        }
    }
}
