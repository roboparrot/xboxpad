﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Pad.Xbox
{
    public class XboxButton : Button
    {
        public XboxButton(string name, string fullName, int flag) : base(name, fullName)
        {
            Flag = flag;
        }

        public int Flag { get; private set; }
    }
}
