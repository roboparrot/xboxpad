﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using SharpDX.XInput;

namespace Control.Pad.Xbox
{
    public class XboxPadController : PadController
    {
        private XboxPad _pad;
        public override Pad Pad
        {
            get { return _pad; }
        }

        private Dictionary<XboxButton, bool> _buttons = new Dictionary<XboxButton, bool>();
        public override ReadOnlyDictionary<Button, bool> Buttons =>
            _buttons.ToDictionary(x => (Button)x.Key, y => y.Value).AsReadOnly();

        private Dictionary<XboxAnalog, double> _analogs = new Dictionary<XboxAnalog, double>();
        public override ReadOnlyDictionary<Analog, double> Analogs =>
            _analogs.ToDictionary(x => (Analog)x.Key, y => y.Value).AsReadOnly();





        private DispatcherTimer _timer = new DispatcherTimer();

        public XboxPadController(XboxPad pad)
        {
            _pad = pad;

            foreach (var button in _pad.XboxButtons)
            {
                _buttons.Add(button, false);
            }

            foreach (var analog in _pad.XboxAnalogs)
            {
                _analogs.Add(analog, default(double));
            }

            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(5) };
            _timer.Tick += _timer_Tick;
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            var state = _pad.Controller.GetState();

            foreach (var flag in Enum.GetValues(typeof(GamepadButtonFlags)))
            {
                var pair = _buttons.SingleOrDefault(x => x.Key.Flag == (short) flag);

                if (((short) state.Gamepad.Buttons & pair.Key.Flag) != 0)
                {//on pressed
                    if (!pair.Value)
                    {
                        Click(new ButtonEventArgs(pair.Key));
                    }
                    _buttons[pair.Key] = true;
                }
                else
                {//on not pressed
                    if (pair.Value)
                    {
                        Release(new ButtonEventArgs(pair.Key));
                    }
                    _buttons[pair.Key] = false;
                }
            }

            var leftTrigger = state.Gamepad.LeftTrigger / 255.0;
            var rightTrigger = state.Gamepad.RightTrigger / 255.0;
            var leftThumbX = state.Gamepad.LeftThumbX / 32768.0;
            var leftThumbY = state.Gamepad.LeftThumbY / 32768.0;
            var rightThumbX = state.Gamepad.RightThumbX / 32768.0;
            var rightThumbY = state.Gamepad.RightThumbY / 32768.0;


            
            UpdateAnalog("LeftThumbX", leftThumbX);
            UpdateAnalog("LeftThumbY", leftThumbY);
            UpdateAnalog("RightThumbX", rightThumbX);
            UpdateAnalog("RightThumbY", rightThumbY);
            UpdateAnalog("LeftTrigger", leftTrigger);
            UpdateAnalog("RightTrigger", rightTrigger);
        }

        private void UpdateAnalog(string xboxName, double newValue)
        {
            //fixme
            var keyValuePair = _analogs.First(x => x.Key.XboxName == xboxName);
            if (newValue != keyValuePair.Value)
            {
                AnalogChanged(new AnalogEventArgs(keyValuePair.Key, newValue));
                _analogs[keyValuePair.Key] = newValue;
            }
        }

        public override void Start()
        {
            _timer.Start();
        }

        public override void Stop()
        {
            _timer.Stop();
        }
    }
}
